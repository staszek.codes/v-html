/*******************************************************************************
 * @copyright 2021 IDEALIGN Stanisław Gregor
 ******************************************************************************/
import { PluginObject } from 'vue';
/**
 * Vue plugin that registers the `<VHTML>` component.
 *
 * @author Stanisław Gregor, Vue.js
 */
export declare const Plugin: PluginObject<undefined>;
