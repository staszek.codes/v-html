/*******************************************************************************
 * @copyright 2021 IDEALIGN Stanisław Gregor
 ******************************************************************************/

import Vue from 'vue'

import { App } from './App'

new Vue({
  render: h => h(App)
}).$mount('#app')
