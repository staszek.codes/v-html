/*******************************************************************************
 * @copyright 2021 IDEALIGN Stanisław Gregor
 ******************************************************************************/
import Vue from 'vue';
/**
 * Simple functional component that accepts HTML string and renders it as-is.
 * Why? Because `v-html` directive is not always an option 😉
 *
 * @author Stanisław Gregor, Thorsten Lünborg
 *
 * @see https://forum.vuejs.org/t/raw-html-without-a-parent-element-via-v-html/87160
 * @see https://jsfiddle.net/Linusborg/mfqjk5hm/
 */
export declare const VHTML: import("vue/types/vue").ExtendedVue<Vue, {}, {}, {}, {
    html: string;
}>;
