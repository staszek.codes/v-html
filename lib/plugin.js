"use strict";
/*******************************************************************************
 * @copyright 2021 IDEALIGN Stanisław Gregor
 ******************************************************************************/
Object.defineProperty(exports, "__esModule", { value: true });
exports.Plugin = void 0;
const VHTML_1 = require("./VHTML");
/**
 * Vue plugin that registers the `<VHTML>` component.
 *
 * @author Stanisław Gregor, Vue.js
 */
exports.Plugin = {
    install(Vue) {
        Vue.component('VHTML', VHTML_1.VHTML);
    }
};
//# sourceMappingURL=plugin.js.map