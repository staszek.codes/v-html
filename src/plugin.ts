/*******************************************************************************
 * @copyright 2021 IDEALIGN Stanisław Gregor
 ******************************************************************************/

import _Vue, { PluginObject } from 'vue'

import { VHTML } from './VHTML'

/**
 * Vue plugin that registers the `<VHTML>` component.
 *
 * @author Stanisław Gregor, Vue.js
 */
export const Plugin: PluginObject<undefined> = {
  install (Vue: typeof _Vue) {
    Vue.component('VHTML', VHTML)
  }
}
